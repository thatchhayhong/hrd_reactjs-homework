import React, { Component } from "react";
import { Table,Button } from 'react-bootstrap';

class TableComponent extends Component {
    // constructor(props) {
    //     super(props);
    // }
    
  render() {
    return (
      <div>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Gender</th>
            </tr>
          </thead>
          <tbody>
          {this.props.data.map((item, index) => (
          <tr key={index} onClick={()=>this.props.onSelect(index)} style={{color:item.selected?"white":"", backgroundColor:item.selected?"black":""}}>
            <td>{index+1}</td>
            <td>{item.username}</td>
            <td>{item.email}</td>
            <td>{item.gender}</td>
          </tr>
        ))}
          </tbody>
        </Table>
        <Button onClick={()=>this.props.onDelete(this.props.data)} variant="danger">Delete</Button>
      </div>
    );
  }
}
export default TableComponent;
