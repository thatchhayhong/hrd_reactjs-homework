import React, { Component } from "react";
import {Navbar} from "react-bootstrap";

class MyNavBar extends Component {
    // constructor(props){
    //     super(props)
    // }
  render() {
    return (
      <div>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="#home"><h2>{this.props.brand}</h2></Navbar.Brand>
          <div  className="mr-auto">

          </div>
          <span>
              Signed in as : <b>{this.props.name}</b>
          </span>
        </Navbar>
      </div>
    );
  }
}

export default MyNavBar;
