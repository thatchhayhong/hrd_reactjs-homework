import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import React, { Component } from "react";
import TableComponent from "./components/TableComponent";
import { Container, Col, Row, Form, Button, Card } from "react-bootstrap";
import MyNavBar from "./components/MyNavBar";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      accounts: [
        {
          username: "chhayhong",
          email: "chhayhong@gmail.com",
          gender: "male",
          selected: false,
        },
        {
          username: "Bupha",
          email: "Bupha@gmail.com",
          gender: "female",
          selected: false,
        },
        {
          username: "Burry98",
          email: "Berry@gmail.com",
          gender: "female",
          selected: false,
        },
        {
          username: "Sombo168",
          email: "Sombo@gmail.com",
          gender: "male",
          selected: false,
        },
      ],
      name: "chhayhong",
      brandname: "KSHRD Student",
      username: "",
      email: "",
      password: "",
      gender: "female",
      msg: "",
      usernameErr: false,
      passwordErr: false,
      emailErr: false,
    };
  }
  onDelete = () => {
    let deleteRecord = [...this.state.accounts];
    let getSelected = deleteRecord.filter((item) => !item.selected);
    this.setState({
      accounts: getSelected,
    });
  };
  onSelect = (index) => {
    let selectedRecord = [...this.state.accounts];
    selectedRecord[index].selected = !selectedRecord[index].selected;
    this.setState({
      accounts: selectedRecord,
    });
  };
  onSubmit = () => {
    let record = [...this.state.accounts];
    let account = {
      username: this.state.username,
      email: this.state.email,
      password: this.state.password,
      gender: this.state.gender,
    };
    let merge = record.concat(account);
    let pattern = /^\S+@\S+\.[a-z]{3}$/g;
    let result = pattern.test(this.state.email.trim());
    console.log(result)
    if (this.state.username=== "" || this.state.password === "" || result) {
      if (this.state.username === "") {
        this.setState({
          usernameErr: true,
        });
      }  
      if (this.state.password === "") {
        this.setState({
          passwordErr: true,
        });
        if (!result) {
          this.setState({
            emailErr: true,
          });
        } else{
          this.setState({
            emailErr: false,
          });
        }
      } 
      if(this.state.username!== "" && this.state.password !== "" && result) {
        this.setState({
          accounts: merge,
        });
        this.setState({
          username: "",
          email: "",
          password: "",
          gender: "female",
          usernameErr:false,
          passwordErr:false,
          emailErr:false
        });
      }
    }
  };
  render() {
    return (
      <div>
        <Container>
          <MyNavBar brand={this.state.brandname} name={this.state.name} />
          <Row>
            <Col sm={4}>
              <Card>
                <Card.Img
                  variant="top"
                  src="https://www.attendit.net/images/easyblog_shared/July_2018/7-4-18/b2ap3_large_totw_network_profile_400.jpg"
                />
              </Card>
              <Form.Group className="m-0">
                        <Form.Control
                  placeholder="Enter username"
                  value={this.state.username}
                  onChange={(e) => this.setState({ username: e.target.value })}
                  type="text"
                />
                <Form.Text className="text-muted">
                  <p className={this.state.usernameErr?"text-danger":"text-primary"}>
                  {this.state.usernameErr
                    ? "Username cannot be emplty!"
                    : "Enter your username"}
                  </p>
                </Form.Text>
                        <Form.Control
                  placeholder="Enter email"
                  value={this.state.email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                  type="email"
                />
                <Form.Text className="text-muted">
                  <p className={this.state.emailErr?"text-danger":"text-primary"}>{this.state.emailErr
                    ? "Email invalid!"
                    : "Enter your email"}</p>
                </Form.Text>
                        <Form.Control
                  placeholder="Enter password"
                  value={this.state.password}
                  onChange={(e) => this.setState({ password: e.target.value })}
                  type="password"
                />
                <Form.Text className="text-muted">
                  <p className={this.state.usernameErr?"text-danger":"text-primary"}>{this.state.usernameErr
                    ? "Password cannot be emplty!"
                    : "Enter your password"}</p>
                </Form.Text>
                <Form.Check
                  custom
                  inline
                  label="Male"
                  type="radio"
                  id="male"
                  name="gender"
                  onChange={(e) => this.setState({ gender: e.target.id })}
                  defaultChecked={this.state.gender === "male" ? true : false}
                />
                <Form.Check
                  custom
                  inline
                  label="Female"
                  type="radio"
                  id="female"
                  name="gender"
                  onChange={(e) => this.setState({ gender: e.target.id })}
                  defaultChecked={this.state.gender === "female" ? true : false}
                />
                <br></br>
                <Button
                  className="mt-3"
                  variant="outline-success"
                  onClick={this.onSubmit}
                >
                  Save
                </Button>
              </Form.Group>
            </Col>

            <Col sm={8}>
              <h3>Student Accounts</h3>
              <TableComponent
                onDelete={this.onDelete}
                onSelect={this.onSelect}
                data={this.state.accounts}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
